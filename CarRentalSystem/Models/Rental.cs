﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CarRentalSystem.Models
{
    public class Rental
    {
        public int RentalId { get; set; }
        [DisplayName("Pickup Date")]
        public DateTime PickupDate { get; set; }
        [DisplayName("Return Date")]
        public DateTime ReturnDate { get; set; }
        [DisplayName("From")]
        public string Source { get; set; }
        [DisplayName("To")]
        public string Destination { get; set; }
        [DisplayName("Fair Amount")]
        public double FairAmount { get; set; }
        [DisplayName("Is Tank Filled")]
        public string InitialFuel { get; set; }

        public List<Car> RentalCarList { get; set; }

       
       
        public int RentalCustomerId { get; set; }

        public Customer RentalCustomer { get; set; }

        public int AdminId { get; set; }
        public Admin RentalAdmin { get; set; }


        
        public int RentalOwnerId { get; set; }
        public Owner RentalOwner { get; set; }
    }
}