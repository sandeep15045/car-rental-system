﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CarRentalSystem.Models
{
    public class Car
    {
        public int CarId { get; set; }
        [DisplayName("Car Name")]
        public string CarName { get; set; }
        [DisplayName("Type Of Car")]
        public string CarType { get; set; }
        [DisplayName("Default Price of Car")]
        public double DefaultPrice { get; set; }
        [DisplayName("Availability of Car")]
        public bool IsAvailable { get; set; }

        
        [DisplayName("Car Number")]
        public string CarNumber { get; set; }

        [DisplayName("Car Image")]
        public string CarImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [DisplayName("Car rating")]
        [Range(1, 5)]
        public int? Ratings { get; set; }
        public double? TotalEarnings { get; set; }

        
        public int OwnerId { get; set; }
        public Owner CarOwner { get; set; }

       
        public int? CarRentalId { get; set; }
        public Rental CarRental { get; set; }

      
        public int AdminId { get; set; }
        public Admin CarAdmin { get; set; }

       
        public int? CarCustomerID { get; set; }
        public Customer CarCustomer { get; set; }
    }
}