﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CarRentalSystem.Models
{
    public class Admin
    {
        #region properties
        public int AdminId { get; set; }
        public string AdminName { get; set; }
        public string Address { get; set; }
        public long PhoneNumber { get; set; }
        // [RegularExpression("[A-Za-z]*[0-9]*[@gmail.com]",ErrorMessage ="Wrong Email Address")]
        
        public string EmailId { get; set; }
        [Required]
        // [RegularExpression("[A-Za-z]*[0-9]*[@|#|&]*",ErrorMessage ="Password is not in correct Format")]
        public string Password { get; set; }

        [DisplayName("Admin Image")]
        public string AdminImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }
        public List<Customer> AdminCustomerList { get; set; }
        public List<Owner> AdminOwnerList { get; set; }
        public List<Car> AdminCarList { get; set; }
        public List<Rental> AdminRentalList { get; set; }
        #endregion

    }
}