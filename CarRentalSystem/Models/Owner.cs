﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CarRentalSystem.Models
{
    public class Owner
    {
       
        public int OwnerId { get; set; }
        [DisplayName("Name of the owner")]
        public string OwnerName { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }

        
        [DisplayName("Phone Number")]
        public long PhoneNumber { get; set; }

        
        [DisplayName("Email Id")]
        public string EmailId { get; set; }
        public string Password { get; set; }
        [DisplayName("Total Earnings")]
        public double? TotalEarnings { get; set; }

        [DisplayName("Owner Image")]
        public string OwnerImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [DisplayName("Owner Cars")]
        public List<Car> OwnerCarList { get; set; }

        [ForeignKey("OwnerAdmin")]
        public int AdminId { get; set; }
        public Admin OwnerAdmin { get; set; }


        public List<Rental> OwnerRentalList { get; set; }
    }
}