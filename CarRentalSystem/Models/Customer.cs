﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CarRentalSystem.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        [DisplayName("Customer Name")]
        public string CustomerName { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Phone Number")]
       
        public long PhoneNumber { get; set; }

        [DisplayName("EmailId")]
        
        public string EmailId { get; set; }

        [DisplayName("Customer Image")]
        public string CustomerImagePath { get; set; }

        [NotMapped]
        public HttpPostedFileBase CustomerImageFile { get; set; }

        public string Password { get; set; }

        

        public int AdminId { get; set; }
        public Admin CustomerAdmin { get; set; }

        /*  public int? CarId { get; set; }
          public Car CustomerCar { get; set; }*/

        public List<Car> CustomerCarList { get; set; }
        public List<Rental> CustomerRentalList { get; set; }
    }
}