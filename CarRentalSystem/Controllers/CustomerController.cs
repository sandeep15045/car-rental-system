﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarRentalSystem.Models;
using CarRentalSystem.Proxy;

namespace CarRentalSystem.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        private CustomerProxy customerProxy;
        private CarProxy carProxy;
        private RentalProxy rentalProxy;
        private MailGeneration mailProxy;
        #region AddCustomer
        public ActionResult AddCustomer()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCustomer(Customer customer)
        {
            string fileName = Path.GetFileNameWithoutExtension(customer.CustomerImageFile.FileName);
            string extension = Path.GetExtension(customer.CustomerImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            customer.CustomerImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            customer.CustomerImageFile.SaveAs(fileName);

            if (ModelState.IsValid)
            {
                mailProxy = new MailGeneration();
                TempData["Customer"] = customer;
                TempData["Email"] = customer.EmailId;
                bool result = false;
                int otpValue = new Random().Next(100000, 999999);
                string email = customer.EmailId;
                string eSubject = "Car Rental System";
                string eBody = "OTP="+otpValue.ToString();
                TempData["OTP"] = otpValue;
                result = mailProxy.SendEmail(email, eSubject, eBody);
                if (result == true)
                {
                    return RedirectToAction("AfterRegister");
                }
            }
            return View();
        }
        public ActionResult AfterRegister()
        {
            ViewBag.EmailId = TempData["Email"].ToString();
            return View();
        }
        [HttpPost]
        public ActionResult AfterRegister(string OTP)
        {

            if (OTP == TempData["OTP"].ToString())
            {

                customerProxy = new CustomerProxy();
                Customer cust = (Customer)TempData["Customer"];
                string msg = customerProxy.AddCustomer(cust);

                if (msg == "Added")
                {
                    ViewBag.Message = "Registered successfully";
                    return RedirectToAction("Login");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.failedMessage = "OTP not correct.Try again";
                return View();
            }


        }
        #endregion

        #region Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Customer customer)
        {

            customerProxy = new CustomerProxy();
            var cust = customerProxy.GetCustomerByEmailId(customer.EmailId);
            if (cust != null)
            {

                if (cust.Password == customer.Password)
                {
                    
                    {
                        TempData["CustomerId"] = cust.CustomerId;
                        ViewBag.obj = cust;
                        
                       return RedirectToAction("DisplayHome", new { id = cust.CustomerId });
                    }
                }
                else
                {
                    return RedirectToAction("InvalidLoginDetails");
                }
            }
            else
            {
                return RedirectToAction("InvalidLoginDetails");
            }
            
        }
        public ActionResult InvalidLoginDetails()
        {
            return View();
        }
        #region Reset Password
        public ActionResult SendEmail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendEmail(Customer c)
        {
            customerProxy = new CustomerProxy();
            mailProxy = new MailGeneration();
            var customer = customerProxy.GetCustomerByEmailId(c.EmailId);
            if (customer != null)
            {
                TempData["Customer"] = customer;
                TempData["Email"] = customer.EmailId;
                bool result = false;
                int otpValue = new Random().Next(100000, 999999);
                string email = customer.EmailId;
                string eSubject = "Car Rental System";
                string eBody = "OTP=" + otpValue.ToString();
                TempData["OTP"] = otpValue;
                result = mailProxy.SendEmail(email, eSubject, eBody);
                if (result == true)
                {
                    return RedirectToAction("OtpValidation");
                }
                else
                {
                    return Content("Can't able to send Email");
                }
            }
            else
            {
                ViewBag.Failed = "Email not present Please try again";
                return View();
            }
        }
        public ActionResult OtpValidation()
        {
            string email = TempData["Email"].ToString();
            ViewBag.Email = email;
            TempData["Email"] = email;
            return View();
        }
        [HttpPost]
        public ActionResult OtpValidation(string otp)
        {
            if (otp == TempData["OTP"].ToString())
            {
                customerProxy = new CustomerProxy();
                var customer = customerProxy.GetCustomerByEmailId(TempData["Email"].ToString());
                return RedirectToAction("ResetPassword", new { id = customer.CustomerId });
            }
            else
            {
                ViewBag.Fail = "Incorrect OTP try again";
                return View();
            }
        }
        public ActionResult ResetPassword(int id)
        {
            customerProxy = new CustomerProxy();
            var customer = customerProxy.GetCustomerById(id);
            return View(customer);
        }
        [HttpPost]
        public ActionResult ResetPassword(Customer c, string newPassword, string confirmPassword)
        {
            if (newPassword == confirmPassword)
            {
                customerProxy = new CustomerProxy();
                var customer = customerProxy.GetCustomerByEmailId(c.EmailId);
                customer.Password = confirmPassword;
                string msg = customerProxy.EditCustomer(customer );
                if (msg == "Edited")
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.Password = "Password does not match please try again";
                return View();
            }
        }
        #endregion
        #endregion
        #region CustomerDetails
        public ActionResult CustomerDetails(int id)
        {
            customerProxy = new CustomerProxy();
            var customer = customerProxy.GetCustomerById(id);
            return View(customer);
        }
        #endregion

        #region EditCustomer
        public ActionResult Edit(int id)
        {
            customerProxy = new CustomerProxy();
            var customer = customerProxy.GetCustomerById(id);
            TempData["CustomerId"] = id;
            return View(customer);
        }
        [HttpPost]
        public ActionResult Edit(Customer newCustomer)
        {
            customerProxy = new CustomerProxy();
            int id = Convert.ToInt32(TempData["CustomerId"]);
            var customer = customerProxy.GetCustomerById(id);
            string msg = "";
            if (ModelState.IsValid)
            {
                customer.CustomerName = newCustomer.CustomerName;
                customer.EmailId = newCustomer.EmailId;
                customer.PhoneNumber = newCustomer.PhoneNumber;
                customer.Password = newCustomer.Password;
                customer.Address = newCustomer.Address;
                customer.CustomerImageFile = newCustomer.CustomerImageFile;


                string fileName = Path.GetFileNameWithoutExtension(customer.CustomerImageFile.FileName);
                string extension = Path.GetExtension(customer.CustomerImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                customer.CustomerImagePath = "~/Images/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
                customer.CustomerImageFile.SaveAs(fileName);

                msg = customerProxy.EditCustomer(customer);
              }
            if (msg == "Edited")
            {
                return RedirectToAction("DisplayHome", new { id = customer.CustomerId });
            }
            else
            {
                return Content(msg);
            }
        }
        #endregion

        #region DisplayHome
        public ActionResult DisplayHome(int id)
        {
            customerProxy = new CustomerProxy();
            ViewBag.CustomerId = id;
            var customer = customerProxy.GetCustomerById(id);
            ViewBag.Image = customer.CustomerImagePath;
            carProxy = new CarProxy();
            var cars = carProxy.GetAvailableCars();
            return View(cars);

        }
        #endregion

        #region Book
        public ActionResult Book(int Cid, int Carid)
        {
            TempData["CustomerId"] = Cid;
            carProxy = new CarProxy();
            var car = carProxy.GetCarDetails(Carid);
            var price = car.DefaultPrice;
            TempData["Price"] = price;
            TempData["OwnerId"] = car.OwnerId;
            TempData["CarId"] = car.CarId;
            return View();

        }
        [HttpPost]
        public ActionResult Book(Rental rent)
        {


            rentalProxy = new RentalProxy();
            carProxy = new CarProxy();

            TimeSpan ts = rent.ReturnDate.Subtract(rent.PickupDate);
            int days = ts.Days;
            double amount = 0;

            if (rent.InitialFuel == "1")
            {
                amount = (days * Convert.ToDouble(TempData["Price"].ToString())) + 1000;
            }
            else
            {
                amount = (days * Convert.ToDouble(TempData["Price"].ToString()));
            }

            rent.FairAmount = amount;

            rent.RentalCustomerId = Convert.ToInt32(TempData["CustomerId"]);
            rent.RentalOwnerId = Convert.ToInt32(TempData["OwnerId"]);
            string msg = rentalProxy.AddRental(rent, Convert.ToInt32(TempData["CarId"]));
            if (msg == "Added")
            {
                ViewBag.CustomerId = Convert.ToInt32(TempData["CustomerId"]);
                return View("Booked");
            }
            else
            {
                return Content(msg);
            }
        }

        #endregion

        #region ReviewCar
        public ActionResult ReviewCar(int Cid)
        {
            rentalProxy = new RentalProxy();

            int id_temp = Cid;
            ViewBag.CustomerId = Cid;
            var rentalList = rentalProxy.GetRentalListByCustomerID(Cid);
            return View(rentalList);
        }

        public ActionResult RatingCar(int Rid, int Cid)
        {
            carProxy = new CarProxy();
            ViewBag.CustomerId = Cid;
            TempData["CustomerId"] = Cid;

            var car_temp = carProxy.GetCarFromRentalId(Rid);
            TempData["CarId"] = car_temp.CarId;
            return View(car_temp);
        }
        [HttpPost]
        public ActionResult RatingCar(Car car)
        {
            carProxy = new CarProxy();
            int id = Convert.ToInt32(TempData["CarId"]);
            var car_temp = carProxy.GetCarDetails(id);
            if (ModelState.IsValid)
            {
                car_temp.Ratings = car.Ratings;
                string msg = carProxy.EditCar(car_temp);
                if (msg == "Edited")
                {
                    int Cid = Convert.ToInt32(TempData["CustomerId"]);
                    ViewBag.CustomerId = Cid;
                    return View("RatedSuccessfully");
                }
                else 
                {
                    return Content(msg);
                }
                
            }
            return View("RatedFailed");
        }
        #endregion

        #region CancelBooking
        public ActionResult CancelBooking(int Cid)
        {
            rentalProxy = new RentalProxy();
            int id_temp = Cid;
            ViewBag.CustomerId = Cid;
            var rentalList = rentalProxy.GetRentalToRemove(Cid);
            return View(rentalList);

        }
        public ActionResult Cancel(int Rid, int Cid)
        {
            rentalProxy = new RentalProxy();
            carProxy = new CarProxy();
            var rental = rentalProxy.GetRentalById(Rid);
            var car = carProxy.GetCarFromRentalId(Rid);
            ViewBag.CustomerId = Cid;

            if (ModelState.IsValid)
            {
                car.IsAvailable = true;
                car.CarRentalId = null;
                car.CarCustomerID = null;
                string msg = carProxy.EditCar(car);
                if (msg == "Edited")
                {
                    
                    string Removemsg = rentalProxy.RemoveRental(rental.RentalId);
                    if (Removemsg == "Removed")
                    {
                        
                        return View("RemovedSuccessfully");
                    }
                    else
                    {
                        return Content("Remove Rental=>" + Removemsg);
                    }
                    
                }
                else
                {
                    return Content("Car Edited=>" + msg);
                }
               
            }
            return View("RemovedFailed");
        }
        #endregion
    }
}