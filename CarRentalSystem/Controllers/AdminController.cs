﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarRentalSystem.Models;
using CarRentalSystem.Proxy;
namespace CarRentalSystem.Controllers
{
    public class AdminController : Controller
    {
        private CarProxy carProxy;
        private OwnerProxy ownerProxy;
        private CustomerProxy customerProxy;
        private RentalProxy rentalProxy;
        private AdminProxy adminProxy;
        private MailGeneration mailProxy;
        // GET: Admin
        #region login
        public ActionResult Login()
        {
            adminProxy = new AdminProxy();
            adminProxy.AddAdmin();
            return View();
        }
        [HttpPost]
        public ActionResult Login(Admin a)
        {
            adminProxy = new AdminProxy();
            var admin = adminProxy.GetAdminByEmailId(a.EmailId);
            if (a.Password == admin.Password)
            {
                return RedirectToAction("ViewCarList");
            }
            else
            {
                return RedirectToAction("InvalidLoginDetails");
            }
        }
        public ActionResult InvalidLoginDetails()
        {
            return View();
        }
        #region AdminDetails
        public ActionResult AdminDetails(int id)
        {
            adminProxy = new AdminProxy();
            var admin = adminProxy.GetAdminById(id);
            return View(admin);
        }
        #endregion
        #region EditAdmin
        public ActionResult Edit(int id)
        {
            adminProxy = new AdminProxy();
            var admin = adminProxy.GetAdminById(id);
            TempData["AdminId"] = id;
            return View(admin);
        }
        [HttpPost]
        public ActionResult Edit(Admin a)
        {
            adminProxy = new AdminProxy();
            var admin = adminProxy.GetAdminById(Convert.ToInt32(TempData["AdminId"]));
            admin.AdminName = a.AdminName;
            admin.EmailId = a.EmailId;
            admin.PhoneNumber = a.PhoneNumber;
            admin.Address = a.Address;
            admin.ImageFile = a.ImageFile;
            string fileName = Path.GetFileNameWithoutExtension(admin.ImageFile.FileName);
            string extension = Path.GetExtension(admin.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            admin.AdminImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            admin.ImageFile.SaveAs(fileName);
            string msg = adminProxy.EditAdmin(admin);
            if (msg == "Edited")
            {
                return RedirectToAction("Edited");
            }
            else
            {
                return Content(msg);
            }
        }
        #endregion
        #region Reset Password
        public ActionResult SendEmail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendEmail(Admin a)
        {
            adminProxy = new AdminProxy();
            mailProxy = new MailGeneration();
            var admin = adminProxy.GetAdminByEmailId(a.EmailId);
            if (admin != null)
            {
                TempData["Admin"] = admin;
                TempData["Email"] = admin.EmailId;
                bool result = false;
                int otpValue = new Random().Next(100000, 999999);
                string email = admin.EmailId;
                string eSubject = "Car Rental System";
                string eBody = "OTP=" + otpValue.ToString();
                TempData["OTP"] = otpValue;
                result = mailProxy.SendEmail(email, eSubject, eBody);
                if (result == true)
                {
                    return RedirectToAction("OtpValidation");
                }
                else
                {
                    return Content("Can't able to send Email");
                }
            }
            else
            {
                ViewBag.Failed = "Email not present Please try again";
                return View();
            }
        }
        public ActionResult OtpValidation()
        {
            string email = TempData["Email"].ToString();
            ViewBag.Email = email;
            TempData["Email"] = email;
            return View();
        }
        [HttpPost]
        public ActionResult OtpValidation(string otp)
        {
            if (otp == TempData["OTP"].ToString())
            {
                adminProxy = new AdminProxy();
                var admin = adminProxy.GetAdminByEmailId(TempData["Email"].ToString());
                return RedirectToAction("ResetPassword", new { id = admin.AdminId });
            }
            else
            {
                ViewBag.Fail = "Incorrect OTP try again";
                return View();
            }
        }
        public ActionResult ResetPassword(int id)
        {
            adminProxy = new AdminProxy();
            var admin = adminProxy.GetAdminById(id);
            return View(admin);
        }
        [HttpPost]
        public ActionResult ResetPassword(Admin a, string newPassword, string confirmPassword)
        {
            if (newPassword == confirmPassword)
            {
                adminProxy = new AdminProxy();
                var admin = adminProxy.GetAdminByEmailId(a.EmailId);
                admin.Password = confirmPassword;
                string msg = adminProxy.EditAdmin(admin);
                if (msg == "Edited")
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.Password = "Password does not match please try again";
                return View();
            }
        }
        #endregion
        #endregion
        #region NavBar
        public ActionResult NavBar()
        {
            return View();
        }
        #endregion
        #region ViewCarModel
        #region Viewcar
        public ActionResult ViewCarList()
        {
            adminProxy=new AdminProxy();
            carProxy = new CarProxy();
            var carList = carProxy.GetCarList();
            var admin = adminProxy.GetAdminById(1001);
            ViewBag.Image = admin.AdminImagePath;
            return View(carList);
        }
        #endregion
        #region DeleteCar
        public ActionResult RemoveCar(int Cid, int Oid)
        {
            carProxy = new CarProxy();

            var carId = carProxy.GetCarDetails(Cid);

            if (carId.IsAvailable == true)
            {

                string msg = carProxy.RemoveCar(Cid);
                if (msg == "Removed")
                {
                    return RedirectToAction("Removed");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.id = Oid;
                return View();
            }
        }
        #endregion
        #region EditCar
        public ActionResult EditCar(int id)
        {
            carProxy = new CarProxy();
            var car = carProxy.GetCarDetails(id);
            return View(car);
        }
        [HttpPost]
        public ActionResult EditCar(Car car)
        {
            carProxy = new CarProxy();
            var c = carProxy.GetCarDetails(car.CarId);
            c.IsAvailable = car.IsAvailable;
            c.DefaultPrice = car.DefaultPrice;
            string msg = carProxy.EditCar(c);
            if (msg == "Edited")
            {
                return RedirectToAction("Edited");
            }
            else
            {
                return Content(msg);
            }
        }
        #region Edited
        public ActionResult Edited()
        {
            return View();
        }
        #endregion
        #region Removed
        public ActionResult Removed()
        {
            return View();
        }
        #endregion
        #region ViewOwnerDetails
        public ActionResult ViewOwnerDetails(int id)
        {
            ownerProxy = new OwnerProxy();
            var o = ownerProxy.GetOwnerData(id);
            return View(o);
        }
        #endregion

        #region ViewCustomerDetails
        public ActionResult ViewCustomerDetails(int id)
        {
            customerProxy = new CustomerProxy();
            var customer = customerProxy.GetCustomerById(id);
            return View(customer);
        }
        #endregion
        #endregion
        #endregion
        
        #region ViewOwnerModel
        #region ViewOwner
        public ActionResult ViewOwnerList()
        {
            adminProxy = new AdminProxy();
            ownerProxy = new OwnerProxy();
            var ownerList = ownerProxy.GetOwnerList();
            var admin = adminProxy.GetAdminById(1001);
            ViewBag.Image = admin.AdminImagePath;
            return View(ownerList);
        }
        #endregion
        #region ViewCar
        public ActionResult ViewCar(int id)
        {
            carProxy = new CarProxy();
            var car = carProxy.GetCarDetails(id);
            return View(car);
        }
        #endregion
        #endregion

        #region ViewCustomerModel
        #region ViewCustomerList
        public ActionResult ViewCustomerList()
        {
            adminProxy = new AdminProxy();
            customerProxy = new CustomerProxy();
            var admin = adminProxy.GetAdminById(1001);
            ViewBag.Image = admin.AdminImagePath;
            var customer = customerProxy.GetCustomerList();
            return View(customer);
        }
        #endregion
        #region ViewBookingList
        public ActionResult BookingList(int id)
        {
            rentalProxy = new RentalProxy();
            var rentalList = rentalProxy.GetRentalListByCustomerID(id);
            return View(rentalList);
        }
        #endregion
        #endregion

        #region TotalPayout
        public ActionResult TotalPayout()
        {
            adminProxy = new AdminProxy();
            ownerProxy = new OwnerProxy();
            var admin = adminProxy.GetAdminById(1001);
            ViewBag.Image = admin.AdminImagePath;
            rentalProxy = new RentalProxy();
            var ownerList = ownerProxy.GetOwnerList();
            return View(ownerList);
        }
        #endregion
    }
}