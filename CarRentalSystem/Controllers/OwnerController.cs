﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using CarRentalSystem.Models;
using CarRentalSystem.Proxy;


namespace CarRentalSystem.Controllers
{
    public class OwnerController : Controller
    {
        private OwnerProxy proxy;
        private CarProxy carProxy;
        private MailGeneration mailProxy;
       
        #region Login
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Owner o)
        {

            proxy = new OwnerProxy();
            var owner = proxy.GetOwnerByEmailId(o.EmailId);
            if (owner != null)
            {

                if (owner.Password == o.Password)
                {
                    TempData["OwnerId"] = owner.OwnerId;
                    return RedirectToAction("DisplayCars", new { id = owner.OwnerId });
                }
                else
                {
                    return RedirectToAction("InvalidLoginDetails", "Owner");
                }
            }
            else
            {
                return RedirectToAction("AddOwner");
            }
        }
        public ActionResult InvalidLoginDetails()
        {
            return View();
        }
        #region Reset Password
        public ActionResult SendEmail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendEmail(Owner o)
        {
            proxy = new OwnerProxy();
            mailProxy = new MailGeneration();
            var owner = proxy.GetOwnerByEmailId(o.EmailId);
            if(owner!=null)
            {
                TempData["Owner"] = owner;
                TempData["Email"] = owner.EmailId;
                bool result = false;
                int otpValue = new Random().Next(100000, 999999);
                string email = owner.EmailId;
                string eSubject = "Car Rental System";
                string eBody = "OTP=" + otpValue.ToString();
                TempData["OTP"] = otpValue;
                result = mailProxy.SendEmail(email, eSubject, eBody);
                if (result == true)
                {
                    return RedirectToAction("OtpValidation");
                }
                else
                {
                    return Content("Can't able to send Email");
                }
            }
            else
            {
                ViewBag.Failed = "Email not present Please try again";
                return View();
            }
        }
        public ActionResult OtpValidation()
        {
            string email = TempData["Email"].ToString();
            ViewBag.Email = email;
            TempData["Email"] = email;
            return View();
        }
        [HttpPost]
        public ActionResult OtpValidation(string otp)
        {
            if(otp==TempData["OTP"].ToString())
            {
                proxy = new OwnerProxy();
                var owner = proxy.GetOwnerByEmailId(TempData["Email"].ToString());
                return RedirectToAction("ResetPassword",new {id=owner.OwnerId });
            }
            else 
            {
                ViewBag.Fail = "Incorrect OTP try again";
                return View();
            }
        }
        public ActionResult ResetPassword(int id)
        {
            proxy = new OwnerProxy();
            var owner = proxy.GetOwnerData(id);
            return View(owner);
        }
        [HttpPost]
        public ActionResult ResetPassword(Owner o,string newPassword,string confirmPassword)
        {
            if(newPassword==confirmPassword)
            {
                proxy = new OwnerProxy();
                var owner = proxy.GetOwnerByEmailId(o.EmailId);
                owner.Password = confirmPassword;
                string msg = proxy.EditOwner(owner, owner.OwnerId);
                if(msg=="Edited")
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.Password = "Password does not match please try again";
                return View();
            }
        }
        #endregion
        #endregion


        #region AddNewOwner
        public ActionResult AddOwner()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddOwner(Owner owner) //Add-new-Owner
        {
            mailProxy = new MailGeneration();
            owner.AdminId = 1001;
            if (ModelState.IsValid)
            {
                
                
                #region Image
                string fileName = Path.GetFileNameWithoutExtension(owner.ImageFile.FileName);
                string extension = Path.GetExtension(owner.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                owner.OwnerImagePath = "~/Images/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
                owner.ImageFile.SaveAs(fileName);
                #endregion
                mailProxy = new MailGeneration();
                TempData["Owner"] = owner;
                TempData["Email"] = owner.EmailId;
                bool result = false;
                int otpValue = new Random().Next(100000, 999999);

                string email = owner.EmailId;
                string eSubject = "Car Rental System";
                string eBody = otpValue.ToString();
                TempData["OTP"] = eBody;
                result = mailProxy.SendEmail(email, eSubject, eBody);
                if (result == true)
                {
                    return RedirectToAction("AfterRegister");
                }
                else
                {
                    return Content("Mail Does not send");
                }
            }
            else
            {
                return View();
            }
        }
        public ActionResult AfterRegister()
        {
            ViewBag.EmailId = TempData["Email"].ToString();
            return View();
        }
        [HttpPost]
        public ActionResult AfterRegister(string OTP)
        {

            if (OTP == TempData["OTP"].ToString())
            {

                proxy = new OwnerProxy();
                Owner ow = (Owner)TempData["Owner"];
                string msg = proxy.AddOwner(ow);

                if (msg == "Added successfully")
                {
                    ViewBag.Message = "Registered successfully";
                    return RedirectToAction("Login");
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.failedMessage = "OTP not correct.Try again";
                return View();
            }
        }
            #endregion

            #region OwnerDetails
            public ActionResult OwnerDetails(int id)
        {
            proxy = new OwnerProxy();
            var owner = proxy.GetOwnerData(id);
            return View(owner);
        }
        #endregion
        #region EditOwner
        public ActionResult Edit(int id)
        {
            proxy = new OwnerProxy();
            TempData["OwnerId"] = id;
            var owner = proxy.GetOwnerData(id);
            return View(owner);
        }
        [HttpPost]
        public ActionResult Edit(Owner o)
        {
            proxy = new OwnerProxy();
            int id = Convert.ToInt32(TempData["ownerId"]);
            var owner = proxy.GetOwnerData(id);
            owner.OwnerName = o.OwnerName;
            owner.EmailId = o.EmailId;
            owner.PhoneNumber = o.PhoneNumber;
            owner.Address = o.Address;
            owner.ImageFile = o.ImageFile;
            string fileName = Path.GetFileNameWithoutExtension(owner.ImageFile.FileName);
            string extension = Path.GetExtension(owner.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            owner.OwnerImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            owner.ImageFile.SaveAs(fileName);
            string msg=proxy.EditOwner(owner,id);
            if (msg == "Edited")
            {
                return RedirectToAction("DisplayCars", new { id = id });
            }
            else
            {
                return Content(msg);
            }
        }
        #endregion

        #region AddCars
        public ActionResult AddCars(int id)  //Add cars to the tbl_cars
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public ActionResult AddCars(Car c)
        {
            carProxy = new CarProxy();
            c.AdminId = 1001;
            if (c.CarType == "SUV")
            {
                c.DefaultPrice = 3000;
            }
            else if (c.CarType == "Sedan")
            {
                c.CarType = "Sedan";
                c.DefaultPrice = 2000;
            }
            else if (c.CarType == "Mini")
            {
                c.CarType = "Mini";
                c.DefaultPrice = 1500;
            }
            else if (c.CarType == "Micro")
            {
                c.CarType = "Micro";
                c.DefaultPrice = 1000;
            }
            else
            {
                c.DefaultPrice = 500;
            }

            c.IsAvailable = true;

            #region Image
            string fileName = Path.GetFileNameWithoutExtension(c.ImageFile.FileName);
            string extension = Path.GetExtension(c.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            c.CarImagePath = "~/Images/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Images/"), fileName);
            c.ImageFile.SaveAs(fileName);
            string msg="Not Added";
            #endregion
            if (ModelState.IsValid)
            {
                carProxy = new CarProxy();
                msg = carProxy.AddCar(c);
            }
            if (msg == "Added")
            {
                return RedirectToAction("DisplayCars", "Owner", new { id = c.OwnerId });
            }
            else
            {
                return Content(msg);
            }

        }
        #endregion
        #region Displaycars
        public ActionResult DisplayCars(int id)
        {
            proxy = new OwnerProxy();
            carProxy = new CarProxy();
            ViewBag.Id = id;
            var owner = proxy.GetOwnerData(id);
            ViewBag.Image = owner.OwnerImagePath;
            var carList = carProxy.GetCarListByOwnerId(id);
            if (carList != null)
            {

                return View(carList);
            }
            else
            {

                return RedirectToAction("AddCars", new { id = id });
            }

        }
        #endregion

        #region RemoveCar
        public ActionResult RemoveCar(int Cid, int Oid)
        {
            carProxy = new CarProxy();

            //var carId = context.Cars.Where(i => i.CarId == id).FirstOrDefault();
            var carDetail = carProxy.GetCarDetails(Cid);

            if (carDetail.IsAvailable == true)
            {

                string msg=carProxy.RemoveCar(Cid);
                if (msg == "Removed")
                {
                    return RedirectToAction("DisplayCars", "Owner", new { id = Oid });
                }
                else
                {
                    return Content(msg);
                }
            }
            else
            {
                ViewBag.id = Oid;
                return View();
            }
        }
        #endregion

        #region
        public ActionResult Payout(int id)
        {
            proxy = new OwnerProxy();
            carProxy = new CarProxy();
            double sum = 0;
            var owner = proxy.GetOwnerData(id);

            var carList = carProxy.GetCarListByOwnerId(id);
            foreach (var val in carList)
            {
                sum = (double)(sum + val.TotalEarnings);
            }
            owner.TotalEarnings = sum;
            ViewBag.Earnings = owner.TotalEarnings;
            string msg = proxy.EditOwner(owner,id);
            if (msg == "Edited")
            {
                return View(carList);
            }
            else
            {
                return Content(msg);
            }
        }
        #endregion

    }
}