﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRentalSystemAPI.Services;
using CarRentalSystem.Models;
namespace CarRentalSystem.Proxy
{
    public class CarProxy:ICarProxy
    {
        private CarQueryService service;
        public CarProxy()
        {

        }
        public CarProxy(CarQueryService service)
        {
            this.service = service;
        }
        public List<Car> GetCarListByOwnerId(int ownerId)
        {
            service = new CarQueryService();
            var carList = service.GetCarListByOwnerId(ownerId);
            
            List<Car> ProjectCarList = new List<Car>();
            foreach(var car in carList)
            {
                Car c = new Car();
                c.CarId = car.CarId;
                c.CarName = car.CarName;
                c.CarType = car.CarType;
                c.DefaultPrice = car.DefaultPrice;
                c.IsAvailable = car.IsAvailable;
                c.CarNumber = car.CarNumber;
                c.CarImagePath = car.CarImagePath;
                c.Ratings = car.Ratings;
                c.TotalEarnings = car.TotalEarnings;
                c.OwnerId = car.OwnerId;
                c.CarRentalId = car.CarRentalId;
                c.AdminId = car.AdminId;
                c.CarCustomerID = car.CarCustomerID;
                ProjectCarList.Add(c);
            }
            return ProjectCarList;
        }

        public string AddCar(Car c)
        {
            service = new CarQueryService();
            List<string> carList = new List<string>();
            carList.Add(c.CarName);
            carList.Add(c.CarType);
            carList.Add(c.DefaultPrice.ToString());
            carList.Add(c.IsAvailable.ToString());
            carList.Add(c.CarNumber);
            carList.Add(c.CarImagePath);
            carList.Add(c.AdminId.ToString());
            carList.Add(c.OwnerId.ToString());
            string msg = service.AddCar(carList);
            return msg;
        }
        public Car GetCarDetails(int id)
        {
            service = new CarQueryService();
            var car = service.GetCarDetails(id);
            Car c = new Car();
            c.CarId = car.CarId;
            c.CarName = car.CarName;
            c.CarType = car.CarType;
            c.DefaultPrice = car.DefaultPrice;
            c.IsAvailable = car.IsAvailable;
            c.CarNumber = car.CarNumber;
            c.CarImagePath = car.CarImagePath;
            c.Ratings = car.Ratings;
            c.TotalEarnings = car.TotalEarnings;
            c.OwnerId = car.OwnerId;
            c.CarRentalId = car.CarRentalId;
            c.CarCustomerID = car.CarCustomerID;
            c.AdminId = car.AdminId;
            return c;
        }
        public string RemoveCar(int id)
        {
            service = new CarQueryService();
            string msg = service.RemoveCar(id);
            return msg;
        }
        public List<Car> GetAvailableCars()
        {
            service = new CarQueryService();
            var carList = service.GetAvailableCars();
            List<Car> ProjectCarList = new List<Car>();
            foreach (var car in carList)
            {
                Car c = new Car();
                c.CarId = car.CarId;
                c.CarName = car.CarName;
                c.CarType = car.CarType;
                c.DefaultPrice = car.DefaultPrice;
                c.IsAvailable = car.IsAvailable;
                c.CarNumber = car.CarNumber;
                c.CarImagePath = car.CarImagePath;
                c.Ratings = car.Ratings;
                c.TotalEarnings = car.TotalEarnings;
                c.OwnerId = car.OwnerId;
                c.CarRentalId = car.CarRentalId;
                c.AdminId = car.AdminId;
                c.CarCustomerID = car.CarCustomerID;
                ProjectCarList.Add(c);
            }
            return ProjectCarList;
        }

        public Car GetCarFromRentalId(int rentalId)
        {
            service = new CarQueryService();
            var car = service.GetCarFromRentalId(rentalId);
            Car c = new Car();
            c.CarId = car.CarId;
            c.CarName = car.CarName;
            c.CarType = car.CarType;
            c.DefaultPrice = car.DefaultPrice;
            c.IsAvailable = car.IsAvailable;
            c.CarNumber = car.CarNumber;
            c.CarImagePath = car.CarImagePath;
            c.Ratings = car.Ratings;
            c.TotalEarnings = car.TotalEarnings;
            c.OwnerId = car.OwnerId;
            c.CarRentalId = car.CarRentalId;
            c.CarCustomerID = car.CarCustomerID;
            c.AdminId = car.AdminId;
            return c;
        }
        public string EditCar(Car c)
        {
            service = new CarQueryService();
            List<string> carList = new List<string>();
            carList.Add(c.CarId.ToString());
            carList.Add(c.CarName);
            carList.Add(c.CarType);
            carList.Add(c.DefaultPrice.ToString());
            carList.Add(c.IsAvailable.ToString());
            carList.Add(c.CarNumber);
            carList.Add(c.CarImagePath);
            carList.Add(c.AdminId.ToString());
            carList.Add(c.OwnerId.ToString());
            carList.Add(c.IsAvailable.ToString());
            carList.Add(c.Ratings.ToString());
            carList.Add(c.CarRentalId.ToString());
            carList.Add(c.CarCustomerID.ToString());
            carList.Add(c.TotalEarnings.ToString());
            string msg = service.EditCar(carList);
            return msg;
        }
        public List<Car> GetCarList()
        {
            service = new CarQueryService();
            var carList = service.GetCarList();

            List<Car> ProjectCarList = new List<Car>();
            foreach (var car in carList)
            {
                Car c = new Car();
                c.CarId = car.CarId;
                c.CarName = car.CarName;
                c.CarType = car.CarType;
                c.DefaultPrice = car.DefaultPrice;
                c.IsAvailable = car.IsAvailable;
                c.CarNumber = car.CarNumber;
                c.CarImagePath = car.CarImagePath;
                c.Ratings = car.Ratings;
                c.TotalEarnings = car.TotalEarnings;
                c.OwnerId = car.OwnerId;
                c.CarRentalId = car.CarRentalId;
                c.AdminId = car.AdminId;
                c.CarCustomerID = car.CarCustomerID;
                ProjectCarList.Add(c);
            }
            return ProjectCarList;
        }
    }
}