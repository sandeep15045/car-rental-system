﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRentalSystem.Models;
using CarRentalSystemAPI.Services;
namespace CarRentalSystem.Proxy
{
    public class CustomerProxy:ICustomerProxy
    {
        private CustomerQueryService service;
        public CustomerProxy()
        {

        }
        public CustomerProxy(CustomerQueryService service)
        {
            this.service = service;
        }
        public string AddCustomer(Customer c)
        {
            service = new CustomerQueryService();
            List<string> customerList = new List<string>();
            customerList.Add(c.CustomerName);
            customerList.Add(c.Address);
            customerList.Add(c.PhoneNumber.ToString());
            customerList.Add(c.EmailId);
            customerList.Add(c.CustomerImagePath);
            customerList.Add(c.Password);
            string msg = service.AddCustomer(customerList);
            return msg;
        }

        public Customer GetCustomerByEmailId(string emailId)
        {
            service = new CustomerQueryService();
            var customer = service.GetCustomerByEmailId(emailId);
            Customer c = new Customer();
            c.CustomerId = customer.CustomerId;
            c.CustomerName = customer.CustomerName;
            c.Address = customer.Address;
            c.PhoneNumber = customer.PhoneNumber;
            c.EmailId = customer.EmailId;
            c.CustomerImagePath = customer.CustomerImagePath;
            c.Password = customer.Password;
            c.AdminId = customer.AdminId;
            return c;
        }
        public Customer GetCustomerById(int id)
        {
            service = new CustomerQueryService();
            var customer = service.GetCustomerById(id);
            Customer c = new Customer();
            c.CustomerId = customer.CustomerId;
            c.CustomerName = customer.CustomerName;
            c.Address = customer.Address;
            c.PhoneNumber = customer.PhoneNumber;
            c.EmailId = customer.EmailId;
            c.CustomerImagePath = customer.CustomerImagePath;
            c.Password = customer.Password;
            c.AdminId = customer.AdminId;
            return c;
        }

        public string EditCustomer(Customer c)
        {
            service = new CustomerQueryService();
            List<string> customerList = new List<string>();
            customerList.Add(c.CustomerId.ToString());
            customerList.Add(c.CustomerName);
            customerList.Add(c.Address);
            customerList.Add(c.PhoneNumber.ToString());
            customerList.Add(c.EmailId);
            customerList.Add(c.CustomerImagePath);
            customerList.Add(c.Password);
            string msg = service.EditCustomer(customerList);
            return msg;
        }
        public List<Customer> GetCustomerList()
        {
            service = new CustomerQueryService();
            var customerList = service.GetCustomerList();
            List<Customer> ProjectCustomerList = new List<Customer>();
            foreach (var customer in customerList)
            {
                Customer c = new Customer();
                c.CustomerId = customer.CustomerId;
                c.CustomerName = customer.CustomerName;
                c.Address = customer.Address;
                c.PhoneNumber = customer.PhoneNumber;
                c.EmailId = customer.EmailId;
                c.Password = customer.Password;
                
                c.CustomerImagePath = customer.CustomerImagePath;
                c.AdminId = customer.AdminId;
                ProjectCustomerList.Add(c);
            }
            return ProjectCustomerList;
        }
    }
}