﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystem.Models;
namespace CarRentalSystem.Proxy
{
    interface ICustomerProxy
    {
        string AddCustomer(Customer c);
        Customer GetCustomerByEmailId(string emailId);
        Customer GetCustomerById(int id);
        string EditCustomer(Customer c);
        List<Customer> GetCustomerList();
    }
}
