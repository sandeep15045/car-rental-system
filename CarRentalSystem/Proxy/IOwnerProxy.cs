﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystem.Models;

namespace CarRentalSystem.Proxy
{
    interface IOwnerProxy
    {
        Owner GetOwnerData(int id);
        Owner GetOwnerByEmailId(string emailId);
        string AddOwner(Owner o);

        string EditOwner(Owner o, int id);
        List<Owner> GetOwnerList();
    }
}
