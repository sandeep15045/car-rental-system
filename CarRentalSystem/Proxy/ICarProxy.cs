﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystem.Models;

namespace CarRentalSystem.Proxy
{
    interface ICarProxy
    {
        List<Car> GetCarListByOwnerId(int ownerId);
        string AddCar(Car c);
        Car GetCarDetails(int id);
        string RemoveCar(int id);
        List<Car> GetAvailableCars();
        Car GetCarFromRentalId(int rentalId);
        string EditCar(Car c);
        List<Car> GetCarList();
    }
}
