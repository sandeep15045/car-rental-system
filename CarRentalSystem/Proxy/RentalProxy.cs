﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRentalSystemAPI.Services;
using CarRentalSystem.Models;

namespace CarRentalSystem.Proxy
{
    public class RentalProxy : IRentalProxy
    {
        private RentalQueryService service;
        public RentalProxy()
        {

        }
        public RentalProxy(RentalQueryService service)
        {
            this.service = service;
        }
        public string AddRental(Rental r, int carId)
        {
            service = new RentalQueryService();
            List<string> rentalList = new List<string>();
            rentalList.Add(r.PickupDate.ToString());
            rentalList.Add(r.ReturnDate.ToString());
            rentalList.Add(r.Source);
            rentalList.Add(r.Destination);
            rentalList.Add(r.FairAmount.ToString());
            rentalList.Add(r.InitialFuel);
            rentalList.Add(r.RentalCustomerId.ToString());
            rentalList.Add(r.RentalOwnerId.ToString());
            string msg = service.AddRental(rentalList, carId);
            return msg;

        }
        public List<Rental> GetRentalListByCustomerID(int customerId)
        {
            service = new RentalQueryService();
            var rentalList = service.GetRentalListByCustomerID(customerId);
            List<Rental> projectRentalList = new List<Rental>();
            foreach (var rental in rentalList)
            {
                Rental r = new Rental();
                r.RentalId = rental.RentalId;
                r.PickupDate = rental.PickupDate;
                r.ReturnDate = rental.ReturnDate;
                r.Source = rental.Source;
                r.Destination = rental.Destination;
                r.FairAmount = rental.FairAmount;
                r.InitialFuel = rental.InitialFuel;
                r.RentalCustomerId = rental.RentalCustomerId;
                r.AdminId = rental.AdminId;
                r.RentalOwnerId = rental.RentalOwnerId;
                projectRentalList.Add(r);
            }
            return projectRentalList;
        }
        public Rental GetRentalById(int id)
        {
            service = new RentalQueryService();
            var rental = service.GetRentalById(id);
            Rental r = new Rental();
            r.RentalId = rental.RentalId;
            r.PickupDate = rental.PickupDate;
            r.ReturnDate = rental.ReturnDate;
            r.Source = rental.Source;
            r.Destination = rental.Destination;
            r.FairAmount = rental.FairAmount;
            r.InitialFuel = rental.InitialFuel;
            r.RentalCustomerId = rental.RentalCustomerId;
            r.AdminId = rental.AdminId;
            r.RentalOwnerId = rental.RentalOwnerId;
            return r;
        }
        public string RemoveRental(int id)
        {
            service = new RentalQueryService();
            string msg = service.RemoveRental(id);
            return msg;

        }
        public List<Rental> GetRentalToRemove(int Cid)
        {
            service = new RentalQueryService();
            var rentalList = service.GetRentalToRemove(Cid);
            List<Rental> projectRentalList = new List<Rental>();
            foreach (var rental in rentalList)
            {
                Rental r = new Rental();
                r.RentalId = rental.RentalId;
                r.PickupDate = rental.PickupDate;
                r.ReturnDate = rental.ReturnDate;
                r.Source = rental.Source;
                r.Destination = rental.Destination;
                r.FairAmount = rental.FairAmount;
                r.InitialFuel = rental.InitialFuel;
                r.RentalCustomerId = rental.RentalCustomerId;
                r.AdminId = rental.AdminId;
                r.RentalOwnerId = rental.RentalOwnerId;
                projectRentalList.Add(r);
            }
            return projectRentalList;

        }
        public List<Rental> GetRentalListByOwnerId(int id)
        {
            service = new RentalQueryService();
            var rentalList = service.GetRentalListByOwnerId(id);
            List<Rental> projectRentalList = new List<Rental>();
            foreach (var rental in rentalList)
            {
                Rental r = new Rental();
                r.RentalId = rental.RentalId;
                r.PickupDate = rental.PickupDate;
                r.ReturnDate = rental.ReturnDate;
                r.Source = rental.Source;
                r.Destination = rental.Destination;
                r.FairAmount = rental.FairAmount;
                r.InitialFuel = rental.InitialFuel;
                r.RentalCustomerId = rental.RentalCustomerId;
                r.AdminId = rental.AdminId;
                r.RentalOwnerId = rental.RentalOwnerId;
                projectRentalList.Add(r);
            }
            return projectRentalList;
        }
    }
}