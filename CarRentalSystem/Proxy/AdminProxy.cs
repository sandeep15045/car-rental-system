﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRentalSystemAPI.Services;
using CarRentalSystem.Models;
namespace CarRentalSystem.Proxy
{
    public class AdminProxy:IAdminProxy
    {
        private AdminQueryService service;
        public AdminProxy()
        {

        }
        public AdminProxy(AdminQueryService service)
        {
            this.service = service;
        }
        public string AddAdmin()
        {
            service = new AdminQueryService();
            string msg = service.AddAdmin();
            return msg;
        }
        public Admin GetAdminById(int id)
        {
            service = new AdminQueryService();
            var admin = service.GetAdminById(id);
            Admin a = new Admin();
            a.AdminId = admin.AdminId;
            a.AdminName = admin.AdminName;
            a.Address = admin.Address;
            a.PhoneNumber = admin.PhoneNumber;
            a.EmailId = admin.EmailId;
            a.Password = admin.Password;
            a.AdminImagePath = admin.AdminImagePath;
            return a;
        }
        public Admin GetAdminByEmailId(string emailId)
        {
            service = new AdminQueryService();
            var admin = service.GetAdminByEmailId(emailId);
            Admin a = new Admin();
            a.AdminId = admin.AdminId;
            a.AdminName = admin.AdminName;
            a.Address = admin.Address;
            a.PhoneNumber = admin.PhoneNumber;
            a.EmailId = admin.EmailId;
            a.Password = admin.Password;
            a.AdminImagePath = admin.AdminImagePath;
            return a;
        }
        public string EditAdmin(Admin a)
        {
            service = new AdminQueryService();
            List<string> adminList = new List<string>();
            adminList.Add(a.AdminId.ToString());
            adminList.Add(a.AdminName); 
            adminList.Add(a.Address);
            adminList.Add(a.PhoneNumber.ToString()); 
            adminList.Add(a.EmailId); 
            adminList.Add(a.AdminImagePath);
            adminList.Add(a.Password);
            string msg = service.EditAdmin(adminList);
            return msg;
        }
    }
}