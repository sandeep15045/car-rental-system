﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarRentalSystemAPI.Services;
using CarRentalSystem.Models;

namespace CarRentalSystem.Proxy
{
    public class OwnerProxy:IOwnerProxy
    {
        public OwnerQueryService service;
        public Owner GetOwnerData(int id)
        {
            service = new OwnerQueryService();
            var ownerData = service.GetOwnerData(id);
            Owner ow = new Owner();
            ow.OwnerId = ownerData.OwnerId;
            ow.OwnerName = ownerData.OwnerName;
            ow.PhoneNumber = ownerData.PhoneNumber;
            ow.Address = ownerData.Address;
            ow.EmailId = ownerData.EmailId;
            ow.TotalEarnings = ownerData.TotalEarnings;
            ow.OwnerImagePath = ownerData.OwnerImagePath;
            ow.Password = ownerData.Password;
            ow.AdminId = ownerData.AdminId;
            return ow;
        }
        public Owner GetOwnerByEmailId(string emailId)
        {
            service = new OwnerQueryService();
            var ownerData = service.GetOwnerByEmailId(emailId);
            Owner ow = new Owner();
            ow.OwnerId = ownerData.OwnerId;
            ow.OwnerName = ownerData.OwnerName;
            ow.PhoneNumber = ownerData.PhoneNumber;
            ow.Address = ownerData.Address;
            ow.EmailId = ownerData.EmailId;
            ow.TotalEarnings = ownerData.TotalEarnings;
            ow.OwnerImagePath = ownerData.OwnerImagePath;
            ow.Password = ownerData.Password;
            ow.AdminId = ownerData.AdminId;

            return ow;
        }
        public string AddOwner(Owner o)
        {
            service = new OwnerQueryService();
            List<string> owner = new List<string>();
            owner.Add(o.OwnerId.ToString());
            owner.Add(o.OwnerName);
            owner.Add(o.EmailId);
            owner.Add(o.PhoneNumber.ToString());
            owner.Add(o.Password);
            owner.Add(o.TotalEarnings.ToString());
            owner.Add(o.OwnerImagePath);
            owner.Add(o.AdminId.ToString());
            owner.Add(o.Address);

            string msg = service.AddOwner(owner);
            return msg;
        }

        public string EditOwner(Owner o,int id)
        {
            service = new OwnerQueryService();
            List<string> owner = new List<string>();
            
            owner.Add(o.OwnerName);
            owner.Add(o.EmailId);
            owner.Add(o.PhoneNumber.ToString());
            owner.Add(o.OwnerImagePath);
            owner.Add(o.Address);
            owner.Add(o.TotalEarnings.ToString());
            owner.Add(o.Password);
            string msg=service.EditOwner(owner, id);
            return msg;
        }
        public List<Owner> GetOwnerList()
        {
            service = new OwnerQueryService();
            var ownerList = service.GetOwnerList();
            List<Owner> ProjectOwnerList = new List<Owner>();
            foreach (var owner in ownerList)
            {
                Owner o = new Owner();
                o.OwnerId = owner.OwnerId;
                o.OwnerName = owner.OwnerName;
                o.Address = owner.Address;
                o.PhoneNumber = owner.PhoneNumber;
                o.EmailId = owner.EmailId;
                o.Password=owner.Password;
                o.TotalEarnings=owner.TotalEarnings;
                o.OwnerImagePath = owner.OwnerImagePath;
                o.AdminId = owner.AdminId;
               ProjectOwnerList.Add(o);
            }
            return ProjectOwnerList;
        }
    }
}