﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Services
{
    interface ICarQueryService
    {
        List<Car> GetCarListByOwnerId(int ownerId);
        string AddCar(List<string> carList);
        Car GetCarDetails(int id);
        string RemoveCar(int id);
        List<Car> GetAvailableCars();
        Car GetCarFromRentalId(int rentalId);
        string EditCar(List<string> carList);
        List<Car> GetCarList();
    }
}
