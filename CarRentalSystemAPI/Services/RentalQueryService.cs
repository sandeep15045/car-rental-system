﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.Repository;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Services
{
    public class RentalQueryService:IRentalQueryService
    {
       private RentalRepository repository;
        public RentalQueryService()
        {

        }
        public RentalQueryService(RentalRepository repository)
        {
            this.repository = repository;
        }
        public string AddRental(List<string> rentalList,int carId)
        {
            repository = new RentalRepository();
            Rental r = new Rental();
            r.PickupDate = Convert.ToDateTime(rentalList[0]);
            r.ReturnDate = Convert.ToDateTime(rentalList[1]);
            r.Source = rentalList[2];
            r.Destination = rentalList[3];
            r.FairAmount = Convert.ToDouble(rentalList[4]);
            r.InitialFuel = rentalList[5];
            r.RentalCustomerId = Convert.ToInt32(rentalList[6]);
            r.AdminId = 1001;
            r.RentalOwnerId = Convert.ToInt32(rentalList[7]);
            string msg = repository.AddRental(r,carId);
            return msg;
        }
        public List<Rental> GetRentalListByCustomerID(int customerId)
        {
            repository = new RentalRepository();
            List<Rental> rentalList = repository.GetRentalListByCustomerID(customerId);
            return rentalList;
        }

        public Rental GetRentalById(int id)
        {
            repository = new RentalRepository();
            var rental = repository.GetRentalById(id);
            return rental;
        }
        public string RemoveRental(int id)
        {
            repository = new RentalRepository();
            string msg = repository.RemoveRental(id);
            return msg;
        }
        public List<Rental> GetRentalToRemove(int Cid)
        {
            repository = new RentalRepository();
            var rentalList = repository.GetRentalToRemove(Cid);
            return rentalList;
        }
        public List<Rental> GetRentalListByOwnerId(int id)
        {
            repository = new RentalRepository();
            var rentalList = repository.GetRentalListByOwnerId(id);
            return rentalList;
        }
    }
}
