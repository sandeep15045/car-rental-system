﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
using CarRentalSystemAPI.Repository;
namespace CarRentalSystemAPI.Services
{
    interface IAdminQueryService
    {
        string AddAdmin();
        Admin GetAdminById(int id);
        Admin GetAdminByEmailId(string emailId);
        string EditAdmin(Admin a);
    }
}
