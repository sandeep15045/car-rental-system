﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
using CarRentalSystemAPI.Repository;
namespace CarRentalSystemAPI.Services
{
    public class CustomerQueryService:ICustomerQueryService
    {
        private CustomerRepository repository;
        public CustomerQueryService()
        {

        }
        public CustomerQueryService(CustomerRepository repository)
        {
            this.repository = repository;
        }
        public string AddCustomer(List<string> customerList)
        {
            Customer c = new Customer();
            repository = new CustomerRepository();
            c.CustomerName = customerList[0];
            c.Address = customerList[1];
            c.PhoneNumber = Convert.ToInt64(customerList[2]);
            c.EmailId = customerList[3];
            c.CustomerImagePath = customerList[4];
            c.Password = customerList[5];
            c.AdminId = 1001;
            string msg = repository.AddCustomer(c);

            return msg;
        }

        public Customer GetCustomerByEmailId(string emailId)
        {
            repository = new CustomerRepository();
            var customer = repository.GetCustomerByEmailId(emailId);
            if(customer==null)
            {
                return new Customer();
            }
            else
            {
                return customer;
            }    
        }
        public Customer GetCustomerById(int id)
        {
            repository = new CustomerRepository();
            var customer = repository.GetCustomerById(id);
            if(customer==null)
            {
                return new Customer();
            }
            else
            {
                return customer;
            }
        }
        public string EditCustomer(List<string> customerList)
        {
            Customer c = new Customer();
            repository = new CustomerRepository();
            c.CustomerId = Convert.ToInt32(customerList[0]);
            c.CustomerName = customerList[1];
            c.Address = customerList[2];
            c.PhoneNumber = Convert.ToInt64(customerList[3]);
            c.EmailId = customerList[4];
            c.CustomerImagePath = customerList[5];
            c.Password = customerList[6];
            c.AdminId = 1001;
            string msg = repository.EditCustomer(c);

            return msg;
        }
        public List<Customer> GetCustomerList()
        {
            repository = new CustomerRepository();
            var customerList = repository.GetCustomerList();
            return customerList;
        }
    }
}
