﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.Repository;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Services
{
    public class AdminQueryService
    {
        private AdminRepository repository;
        public AdminQueryService()
        {

        }
        public AdminQueryService(AdminRepository repository)
        {
            this.repository = repository;
        }
        public string AddAdmin()
        {
            repository = new AdminRepository();
            string msg = repository.AddAdmin();
            return msg;
        }
        public Admin GetAdminById(int id)
        {
            repository = new AdminRepository();
            var admin = repository.GetAdminById(id);
            return admin;
        }
        public Admin GetAdminByEmailId(string emailId)
        {
            repository = new AdminRepository();
            var admin = repository.GetAdminByEmailId(emailId);
            return admin;
        }
        public string EditAdmin(List<string> adminList)
        {
            repository = new AdminRepository();
            Admin a = new Admin();
            a.AdminId = Convert.ToInt32(adminList[0]);
            a.AdminName = adminList[1];
            a.Address = adminList[2];
            a.PhoneNumber = Convert.ToInt64(adminList[3]);
            a.EmailId = adminList[4];
            a.AdminImagePath = adminList[5];
            a.Password = adminList[6];
            string msg = repository.EditAdmin(a);
            return msg;
        }
    }
}
