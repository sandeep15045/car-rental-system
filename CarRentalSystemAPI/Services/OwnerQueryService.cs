﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
using CarRentalSystemAPI.Repository;
namespace CarRentalSystemAPI.Services
{
    public class OwnerQueryService:IOwnerQueryService
    {
        private OwnerRepository repository;
        public OwnerQueryService()
        {

        }
        public OwnerQueryService(OwnerRepository repository)
        {
            this.repository = repository;
        }

        public Owner GetOwnerData(int id)
        {
            repository = new OwnerRepository();
            var owner = repository.GetOwnerById(id);
            if(owner==null)
            {
                return new Owner();
            }
            return owner;
        }
        public Owner GetOwnerByEmailId(string emailId)
        {
            repository = new OwnerRepository();
            var owner = repository.GetOwnerByEmailId(emailId);
            if(owner==null)
            {
                return new Owner();
            }
            return owner;
        }
        public string AddOwner(List<string> owner)
        {
            repository = new OwnerRepository();
            Owner o = new Owner();
            o.OwnerId = Convert.ToInt32(owner[0]);
            o.OwnerName = owner[1];
            o.EmailId = owner[2];
            o.PhoneNumber = Convert.ToInt64(owner[3]);
            o.Password = owner[4];
            if (owner[5] == "")
            {
                o.TotalEarnings = 0;
            }
            else
            {
                o.TotalEarnings = double.Parse(owner[5]);
            }
            o.OwnerImagePath = owner[6];
            o.AdminId = Convert.ToInt32(owner[7]);
            o.Address = owner[8];
            string msg = repository.AddOwner(o);
            return msg;
        }
        public string EditOwner(List<string>owner,int id)
        {
            repository = new OwnerRepository();
            Owner o = new Owner();
            o.OwnerName = owner[0];
            o.EmailId = owner[1];
            o.PhoneNumber = Convert.ToInt64(owner[2]);
            o.OwnerImagePath = owner[3];
            o.Address = owner[4];
            if(owner[5]==""||owner[5]=="null")
            {
                o.TotalEarnings = null;
            }
            else
            {
                o.TotalEarnings = Convert.ToDouble(owner[5]);
            }
            o.Password = owner[6];
            string msg = repository.EditOwner(o, id);
            return msg;
        }
        public List<Owner> GetOwnerList()
        {
            repository = new OwnerRepository();
            var ownerList = repository.GetOwnerList();
            return ownerList;
        }


    }
}
