﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
using CarRentalSystemAPI.Repository;

namespace CarRentalSystemAPI.Services
{
    public class CarQueryService:ICarQueryService
    {
        private CarRepository repository;
        public CarQueryService()
        {

        }
        public CarQueryService(CarRepository repository)
        {
            this.repository = repository;
        }
        public List<Car> GetCarListByOwnerId(int ownerId)
        {
            repository = new CarRepository();
            var carList = repository.GetCarListByOwnerId(ownerId);
          
            if(carList==null)
            {
                return new List<Car>();
            }
            else
            {
                return carList;
            }
        }
        public string AddCar(List<string>carList)
        {
            repository = new CarRepository();
            Car c = new Car();
            c.CarName = carList[0];
            c.CarType = carList[1];
            c.DefaultPrice = Convert.ToInt32(carList[2]);
            c.IsAvailable = true;
            c.CarNumber = carList[4];
            c.CarImagePath = carList[5];
            c.AdminId = Convert.ToInt32(carList[6]);
            c.OwnerId = Convert.ToInt32(carList[7]);
            string msg = repository.AddCar(c);
            return msg;
        }

        public Car GetCarDetails(int id)
        {
            repository = new CarRepository();
            var car = repository.GetCarDetails(id);
            if(car==null)
            {
                return new Car();
            }
            else
            {
                return car;
            }
        }
        public string RemoveCar(int id)
        {
            repository = new CarRepository();
            string msg = repository.RemoveCar(id);
            return msg;
        }
        public List<Car> GetAvailableCars()
        {
            repository = new CarRepository();
            var carList = repository.GetAvailableCars();
            if (carList == null)
            {
                return new List<Car>();
            }
            else
            {
                return carList;
            }
        }
         public Car GetCarFromRentalId(int rentalId)
         {
            repository = new CarRepository();
            var car = repository.GetCarFromRentalId(rentalId);
            return car;
         }
        public string EditCar(List<string> carList)
        {
            repository = new CarRepository();
            Car c = new Car();
            c.CarId = Convert.ToInt32(carList[0]);
            c.CarName = carList[1];
            c.CarType = carList[2];
            c.DefaultPrice = Convert.ToInt32(carList[3]);
            c.CarNumber = carList[5];
            c.CarImagePath = carList[6];
            c.AdminId = Convert.ToInt32(carList[7]);
            c.OwnerId = Convert.ToInt32(carList[8]);
            c.IsAvailable = Convert.ToBoolean(carList[9]);
            c.Ratings = Convert.ToInt32(carList[10]);
            if (carList[11] == "null" || carList[11] == "")
            {
                c.CarRentalId = null;
            }
            else
            {
                c.CarRentalId = Convert.ToInt32(carList[11]);
            }
            if (carList[12] == "null" || carList[12] == "")
            {
                c.CarCustomerID = null;
            }
            else
            {
                c.CarCustomerID = Convert.ToInt32(carList[12]);
            }
            c.TotalEarnings = Convert.ToDouble(carList[13]);
            string msg = repository.EditCar(c);
            return msg;
        }
        public List<Car> GetCarList()
        {
            repository = new CarRepository();
            var carList = repository.GetCarList();
            return carList;
        }
    }
}
