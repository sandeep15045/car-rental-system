﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;

namespace CarRentalSystemAPI.Services
{
    interface ICustomerQueryService
    {
        string AddCustomer(List<string> customerList);
        Customer GetCustomerByEmailId(string emailId);
        Customer GetCustomerById(int id);
        string EditCustomer(List<string> customerList);
        List<Customer> GetCustomerList();
    }
}
