﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
using CarRentalSystemAPI.Repository;

namespace CarRentalSystemAPI.Services
{
    interface IOwnerQueryService
    {
        Owner GetOwnerData(int id);
        Owner GetOwnerByEmailId(string emailId);
        string AddOwner(List<string>owner);
        string EditOwner(List<string> owner, int id);
        List<Owner> GetOwnerList();
    }
}
