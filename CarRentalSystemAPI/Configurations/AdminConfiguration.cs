﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Configurations
{
    class AdminConfiguration : EntityTypeConfiguration<Admin>
    {
        public AdminConfiguration()
        {
            //renaming table name
            ToTable("tbl_Admin");

            //primary key
            HasKey(a => a.AdminId);

            //to remove the identity
            Property(a => a.AdminId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            Property(a => a.AdminName)
                .IsRequired()
                .HasMaxLength(50);

            Property(a => a.Address)
                .HasMaxLength(300);

            Property(a => a.PhoneNumber)
                .IsRequired();

            HasIndex(a => a.PhoneNumber)
                .IsUnique();

            Property(a => a.EmailId)
                .IsRequired()
                .HasMaxLength(100);
           
            HasIndex(a => a.EmailId)
                .IsUnique();


            Property(a => a.Password)
                .IsRequired();
        }
    }
}
