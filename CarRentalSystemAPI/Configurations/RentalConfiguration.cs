﻿using CarRentalSystemAPI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.Configurations
{
    class RentalConfiguration : EntityTypeConfiguration<Rental>
    {
        public RentalConfiguration()
        {
            ToTable("tbl_Rental");

            HasKey(r => r.RentalId);

            Property(r => r.PickupDate)
                .IsRequired();

            Property(r => r.ReturnDate)
                .IsRequired();

            Property(r => r.Source)
                .HasMaxLength(255)
                .IsRequired();

            Property(r => r.Destination)
                .HasMaxLength(255)
                .IsRequired();

            Property(r => r.FairAmount)
                .IsRequired();

            Property(r => r.InitialFuel)
                .HasMaxLength(255)
                .IsRequired();

            HasRequired(r => r.RentalAdmin)
                .WithMany(a => a.AdminRentalList)
                .HasForeignKey(r => r.AdminId)
                .WillCascadeOnDelete(false);

            HasRequired(r => r.RentalCustomer)
                .WithMany(cu => cu.CustomerRentalList)
                .HasForeignKey(r => r.RentalCustomerId)
                .WillCascadeOnDelete(false);

            HasRequired(r => r.RentalOwner)
                .WithMany(o => o.OwnerRentalList)
                .HasForeignKey(r => r.RentalOwnerId)
                .WillCascadeOnDelete(false);
        }

    }
}
