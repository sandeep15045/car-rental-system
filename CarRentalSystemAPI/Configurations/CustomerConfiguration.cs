﻿using CarRentalSystemAPI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.Configurations
{
    class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("tbl_Customer");

            HasKey(c => c.CustomerId);

            Property(a => a.CustomerName)
                .IsRequired()
                .HasMaxLength(50);

            Property(c => c.Address)
                .IsRequired()
                .HasMaxLength(300);

            Property(c => c.PhoneNumber)
                .IsRequired();

            HasIndex(c => c.PhoneNumber)
                .IsUnique();

            Property(c => c.EmailId)
                .IsRequired()
                .HasMaxLength(100);

            HasIndex(c => c.EmailId)
                .IsUnique();


            Property(c => c.Password)
                .IsRequired();

            HasRequired(c => c.CustomerAdmin)
                .WithMany(a => a.AdminCustomerList)
                .HasForeignKey(c => c.AdminId)
                .WillCascadeOnDelete(false);

           
        }
    }
}
