﻿using CarRentalSystemAPI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.Configurations
{
    class CarConfiguration : EntityTypeConfiguration<Car>
    {
        public CarConfiguration()
        {
            ToTable("tbl_Car");

            HasKey(c => c.CarId);

            Property(c => c.CarName)
                .IsRequired()
                .HasMaxLength(255);

            Property(c => c.CarType)
                .IsRequired()
                .HasMaxLength(255);

            Property(c => c.DefaultPrice)
                .IsRequired();

            Property(c => c.CarNumber)
                .IsRequired()
                .HasMaxLength(20);
            
            HasIndex(c => c.CarNumber)
                .IsUnique();

            HasRequired(c => c.CarRental)
                .WithMany(r => r.RentalCarList)
                .HasForeignKey(c => c.CarRentalId)
                .WillCascadeOnDelete(false);

            HasRequired(c => c.CarOwner)
                .WithMany(o => o.OwnerCarList)
                .HasForeignKey(c => c.OwnerId)
                .WillCascadeOnDelete(false);

            HasRequired(c => c.CarAdmin)
                .WithMany(a => a.AdminCarList)
                .HasForeignKey(c => c.AdminId)
                .WillCascadeOnDelete(false);

            HasRequired(c => c.CarCustomer)
                .WithMany(cu => cu.CustomerCarList)
                .HasForeignKey(c => c.CarCustomerID)
                .WillCascadeOnDelete(false);

        }
    }
}
