﻿using CarRentalSystemAPI.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.Configurations
{
    class OwnerConfiguration : EntityTypeConfiguration<Owner>
    {
        public OwnerConfiguration()
        { 
        ToTable("tbl_Owner");

        HasKey(o => o.OwnerId);



        Property(o => o.OwnerName)
                .IsRequired()
                .HasMaxLength(50);

        Property(o => o.Address)
                .IsRequired()
                .HasMaxLength(300);

        Property(o => o.PhoneNumber)
                .IsRequired();

            HasIndex(o => o.PhoneNumber)
                .IsUnique();

        Property(o => o.EmailId)
                .IsRequired()
                .HasMaxLength(100);

            HasIndex(o => o.EmailId)
                .IsUnique();

            Property(o => o.Password)
                .IsRequired();

        HasRequired(o => o.OwnerAdmin)
                .WithMany(a => a.AdminOwnerList)
                .HasForeignKey(o => o.AdminId)
                .WillCascadeOnDelete(false);



        }
    }
}
