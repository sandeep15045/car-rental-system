﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;

namespace CarRentalSystemAPI.Repository
{
    public class OwnerRepository:IOwnerRepository
    {
        private CarContext context;
        public OwnerRepository()
        {

        }
        public OwnerRepository(CarContext context)
        {
            this.context = context;
        }
        public Owner GetOwnerById(int id)
        {
            context = new CarContext();
            var owner = context.Owners.Find(id);
            return owner;
        }
        public Owner GetOwnerByEmailId(string emailId)
        {
            context = new CarContext();
            var owner = context.Owners.Where(o => o.EmailId == emailId).FirstOrDefault();
            return owner;
        }
        public string AddOwner(Owner ow)
        {
            try
            {
                context = new CarContext();
                context.Owners.Add(ow);
                context.SaveChanges();
                return "Added successfully";
            }
            catch(Exception ex)
            {
                
                return ex.InnerException.InnerException.Message;
            }
        }
        public string EditOwner(Owner o,int id)
        {
            context = new CarContext();
            var owner = context.Owners.Find(id);
            owner.OwnerName = o.OwnerName;
            owner.EmailId = o.EmailId;
            owner.PhoneNumber = o.PhoneNumber;
            owner.OwnerImagePath = o.OwnerImagePath;
            owner.Address = o.Address;
            owner.TotalEarnings = o.TotalEarnings;
            owner.Password = o.Password;
            try
            {
                context.SaveChanges();
                return "Edited";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

        }
        public List<Owner> GetOwnerList()
        {
            context = new CarContext();
            var ownerList = context.Owners.ToList();
            return ownerList;
        }
        
    }
}
