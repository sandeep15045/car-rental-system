﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Repository
{
    interface IRentalRepository
    {
        string AddRental(Rental r,int carId);
        List<Rental> GetRentalListByCustomerID(int customerId);
        Rental GetRentalById(int id);
        string RemoveRental(int id);
        List<Rental> GetRentalToRemove(int Cid);
        List<Rental> GetRentalListByOwnerId(int id);
    }
}
