﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;

namespace CarRentalSystemAPI.Repository
{
    interface IAdminRepository
    {
        string AddAdmin();
        Admin GetAdminById(int id);
        Admin GetAdminByEmailId(string emailId);
        string EditAdmin(Admin a);
    }
}
