﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Repository
{
    public class CustomerRepository:ICustomerRepository
    {
        private CarContext context;
        public CustomerRepository()
        {

        }
        public CustomerRepository(CarContext context)
        {
            this.context = context;
        }
        public string AddCustomer(Customer c)
        {
            try
            {
                context = new CarContext();
                context.Customers.Add(c);
                context.SaveChanges();
                return "Added";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        public Customer GetCustomerByEmailId(string emailId)
        {
            context = new CarContext();
            var customer = context.Customers.Where(c => c.EmailId == emailId).FirstOrDefault();
            return customer;
        }
        public Customer GetCustomerById(int id)
        {
            context = new CarContext();
            var customer = context.Customers.Find(id);
            return customer;
        }
        public string EditCustomer(Customer c)
        {
            try
            {
                context = new CarContext();
                var customer = context.Customers.Find(c.CustomerId);
                customer.CustomerName = c.CustomerName;
                customer.Address = c.Address;
                customer.EmailId = c.EmailId;
                customer.PhoneNumber = c.PhoneNumber;
                customer.CustomerImagePath = c.CustomerImagePath;
                customer.Password = c.Password;
                context.SaveChanges();
                return "Edited";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Customer> GetCustomerList()
        {
            context = new CarContext();
            var customerList = context.Customers.ToList();
            return customerList;
        }
    }
}
