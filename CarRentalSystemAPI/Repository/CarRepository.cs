﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;

namespace CarRentalSystemAPI.Repository
{
    public class CarRepository:ICarRepository
    {
        private CarContext context;
        public CarRepository()
        {

        }
        public CarRepository(CarContext context)
        {
            this.context = context;
        }

        public List<Car> GetCarListByOwnerId(int ownerId)
        {
            context = new CarContext();
            var carList = context.Cars.Where(c => c.OwnerId == ownerId).ToList();
            return carList;
        }
        public string AddCar(Car c)
        {
            context = new CarContext();
            try
            {
                context.Cars.Add(c);
                context.SaveChanges();
                return "Added";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
        public Car GetCarDetails(int id)
        {
            context = new CarContext();
            var car = context.Cars.Find(id);
            return car;
        }

        public string RemoveCar(int id)
        {
            context = new CarContext();
            try
            {
                var car = context.Cars.Find(id);
                context.Cars.Remove(car);
                context.SaveChanges();
                return "Removed";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Car> GetAvailableCars()
        {
            context = new CarContext();
            var carList = context.Cars.Where(c => c.IsAvailable == true).ToList();
            return carList;
        }
        public Car GetCarFromRentalId(int rentalId)
        {
            context = new CarContext();
            var car = context.Cars.Where(c => c.CarRentalId == rentalId).FirstOrDefault();
            return car;
        }
        public string EditCar(Car car)
        {
            try
            {
                context = new CarContext();
                var c = context.Cars.Find(car.CarId);
                c.CarId = car.CarId;
                c.CarName = car.CarName;
                c.CarType = car.CarType;
                c.DefaultPrice = car.DefaultPrice;
                c.CarNumber = car.CarNumber;
                c.CarImagePath = car.CarImagePath;
                c.AdminId = car.AdminId;
                c.OwnerId = car.OwnerId;
                c.IsAvailable = car.IsAvailable;
                c.Ratings = car.Ratings;
                c.CarRentalId = car.CarRentalId;
                c.CarCustomerID = car.CarCustomerID;
                c.TotalEarnings = car.TotalEarnings;
                context.SaveChanges();
                return "Edited";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public List<Car> GetCarList()
        {
            context = new CarContext();
            var carList = context.Cars.ToList();
            return carList;
        }
    }
}
