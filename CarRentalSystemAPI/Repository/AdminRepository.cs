﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;

namespace CarRentalSystemAPI.Repository
{
    public class AdminRepository:IAdminRepository
    {
        private CarContext context;
        public AdminRepository()
        {

        }
        public AdminRepository(CarContext context)
        {
            this.context = context;
        }
        public string AddAdmin()
        {
            context = new CarContext();
            Admin admin = new Admin()
            {
                AdminId = 1001,
                AdminName = "Sameer Mittal",
                Address = "Abc Road Banglore",
                PhoneNumber = 0123456789,
                EmailId = "sameer.admin@gmail.com",
                Password = "sameer"
            };
            var ad = context.Admins.Find(admin.AdminId);
            if (ad != null)
            {
                return "Admin Already present";
            }
            else
            {
                context.Admins.Add(admin);
                context.SaveChanges();
                return "Added";
            }
        }
        public Admin GetAdminById(int id)
        {
            context = new CarContext();
            var admin = context.Admins.Find(id);
            return admin;
        }
        public Admin GetAdminByEmailId(string emailId)
        {
            context = new CarContext();
            var admin = context.Admins.Where(a => a.EmailId == emailId).FirstOrDefault();
            return admin;
        }
        public string EditAdmin(Admin a)
        {
            try
            {
                context = new CarContext();
                var admin = context.Admins.Find(a.AdminId);
                admin.AdminName = a.AdminName;
                admin.Address = a.Address;
                admin.PhoneNumber = a.PhoneNumber;
                admin.EmailId = a.EmailId;
                admin.AdminImagePath = a.AdminImagePath;
                admin.Password = a.Password;
                context.SaveChanges();
                return "Edited";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
