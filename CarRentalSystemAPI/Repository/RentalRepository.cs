﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Repository
{
    public class RentalRepository:IRentalRepository
    {
        private CarContext context;
        public RentalRepository()
        {

        }
        public RentalRepository(CarContext context)
        {
            this.context = context;
        }
        public string AddRental(Rental r,int carId)
        {
            context = new CarContext();
            CarRepository repo = new CarRepository();
            List<string> outputList = new List<string>();
            try
            {
                var rental= context.Rentals.Add(r);
                var car = context.Cars.Find(carId);
                car.CarRentalId = rental.RentalId;
                car.CarCustomerID = rental.RentalCustomerId;
                car.IsAvailable = false;
                if(car.TotalEarnings==null || car.TotalEarnings==0)
                {
                    car.TotalEarnings = 0;
                    car.TotalEarnings = rental.FairAmount;
                }
                else
                {
                    car.TotalEarnings += rental.FairAmount;
                }

                context.SaveChanges();

                return "Added";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
        public List<Rental> GetRentalListByCustomerID(int customerId)
        {
            context = new CarContext();
            var rentalList = context.Rentals.Where(r => r.RentalCustomerId == customerId).ToList();
            return rentalList;
        }
        public Rental GetRentalById(int id)
        {
            context = new CarContext();
            var rental = context.Rentals.Find(id);
            return rental;
        }
        public string RemoveRental(int id)
        {
            context = new CarContext();
            try
            {
                var r = context.Rentals.Find(id);
                context.Rentals.Remove(r);
                context.SaveChanges();
                return "Removed";
            }
            catch (Exception ex)
            {
                return ex.InnerException.InnerException.Message;
            }
        }
        public List<Rental> GetRentalToRemove(int Cid)
        {
            context = new CarContext();
            DateTime today = DateTime.Now;
            var rentalList = context.Rentals.Where(r => r.RentalCustomerId == Cid)
                .Where(r => r.PickupDate > today)
                .Where(r => r.ReturnDate > today).ToList();
            return rentalList;
        }
        public List<Rental> GetRentalListByOwnerId(int id)
        {
            context = new CarContext();
            var rentalList = context.Rentals.Where(r => r.RentalOwnerId == id).ToList();
            return rentalList;
        }
    }
}
