﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Repository
{
    interface IOwnerRepository
    {
        Owner GetOwnerById(int id);
        Owner GetOwnerByEmailId(string emailId);
        string AddOwner(Owner ow);
        List<Owner> GetOwnerList();

    }
}
