﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarRentalSystemAPI.DTO;
namespace CarRentalSystemAPI.Repository
{
    interface ICustomerRepository
    {
        string AddCustomer(Customer c);
        Customer GetCustomerByEmailId(string emailId);
        Customer GetCustomerById(int id);
        string EditCustomer(Customer c);
        List<Customer> GetCustomerList();
    }
}
