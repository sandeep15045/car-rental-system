﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.DTO
{
    public class Owner
    {
       
        public int OwnerId { get; set; }
        
        public string OwnerName { get; set; }
       
        public string Address { get; set; }

       
        public long PhoneNumber { get; set; }

       
        public string EmailId { get; set; }
        public string Password { get; set; }
      
        public double? TotalEarnings { get; set; }

        
        public string OwnerImagePath { get; set; }

       
      

        
        public List<Car> OwnerCarList { get; set; }

       
        public int AdminId { get; set; }
        public Admin OwnerAdmin { get; set; }


        public List<Rental> OwnerRentalList { get; set; }
    }
}
