﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.DTO
{
    public class Customer
    {
       
        public int CustomerId { get; set; }
       
        public string CustomerName { get; set; }
       
        public string Address { get; set; }

        
        public long PhoneNumber { get; set; }

       
        public string EmailId { get; set; }

       
        public string CustomerImagePath { get; set; }

        public string Password { get; set; }

        public int AdminId { get; set; }
        public Admin CustomerAdmin { get; set; }
        public List<Car> CustomerCarList { get; set; }
        public List<Rental> CustomerRentalList { get; set; }
    }
}
