﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.DTO
{
    public class Rental
    {
       
        public int RentalId { get; set; }
       
        public DateTime PickupDate { get; set; }
        
        public DateTime ReturnDate { get; set; }
       
        public string Source { get; set; }
        
        public string Destination { get; set; }
        
        public double FairAmount { get; set; }
       
        public string InitialFuel { get; set; }

        public List<Car> RentalCarList { get; set; }

       

       
        public int RentalCustomerId { get; set; }

        public Customer RentalCustomer { get; set; }


        public int AdminId { get; set; }
        public Admin RentalAdmin { get; set; }


        
        public int RentalOwnerId { get; set; }
        public Owner RentalOwner { get; set; }
    }
}
