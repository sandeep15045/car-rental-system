﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.DTO
{
    public class Car
    {
        
        public int CarId { get; set; }
        
        public string CarName { get; set; }
        
        public string CarType { get; set; }
        
        public double DefaultPrice { get; set; }
        
        public bool IsAvailable { get; set; }

        
        public string CarNumber { get; set; }

        
        public string CarImagePath { get; set; }

       

        
        public int? Ratings { get; set; }
        public double? TotalEarnings { get; set; }

       
        public int OwnerId { get; set; }
        public Owner CarOwner { get; set; }

        
        public int? CarRentalId { get; set; }
        public Rental CarRental { get; set; }

        
        public int AdminId { get; set; }
        public Admin CarAdmin { get; set; }

       
        public int? CarCustomerID { get; set; }
        public Customer CarCustomer { get; set; }
    }
}
