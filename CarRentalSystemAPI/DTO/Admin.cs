﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRentalSystemAPI.DTO
{
    public class Admin
    {
        #region properties
        public int AdminId { get; set; }
        public string AdminName { get; set; }
        public string Address { get; set; }
        public long PhoneNumber { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string AdminImagePath { get; set; }

        
        
        public List<Customer> AdminCustomerList { get; set; }
        public List<Owner> AdminOwnerList { get; set; }
        public List<Car> AdminCarList { get; set; }
        public List<Rental> AdminRentalList { get; set; }
        #endregion
    }
}
