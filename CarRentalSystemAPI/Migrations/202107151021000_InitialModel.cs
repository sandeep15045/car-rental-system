﻿namespace CarRentalSystemAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_Admin",
                c => new
                    {
                        AdminId = c.Int(nullable: false),
                        AdminName = c.String(nullable: false, maxLength: 50),
                        Address = c.String(maxLength: 300),
                        PhoneNumber = c.Long(nullable: false),
                        EmailId = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false),
                        AdminImagePath = c.String(),
                    })
                .PrimaryKey(t => t.AdminId)
                .Index(t => t.PhoneNumber, unique: true)
                .Index(t => t.EmailId, unique: true);
            
            CreateTable(
                "dbo.tbl_Car",
                c => new
                    {
                        CarId = c.Int(nullable: false, identity: true),
                        CarName = c.String(nullable: false, maxLength: 255),
                        CarType = c.String(nullable: false, maxLength: 255),
                        DefaultPrice = c.Double(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                        CarNumber = c.String(nullable: false, maxLength: 20),
                        CarImagePath = c.String(),
                        Ratings = c.Int(),
                        TotalEarnings = c.Double(),
                        OwnerId = c.Int(nullable: false),
                        CarRentalId = c.Int(nullable: true),
                        AdminId = c.Int(nullable: false),
                        CarCustomerID = c.Int(nullable: true),
                    })
                .PrimaryKey(t => t.CarId)
                .ForeignKey("dbo.tbl_Admin", t => t.AdminId)
                .ForeignKey("dbo.tbl_Customer", t => t.CarCustomerID)
                .ForeignKey("dbo.tbl_Owner", t => t.OwnerId)
                .ForeignKey("dbo.tbl_Rental", t => t.CarRentalId)
                .Index(t => t.CarNumber, unique: true)
                .Index(t => t.OwnerId)
                .Index(t => t.CarRentalId)
                .Index(t => t.AdminId)
                .Index(t => t.CarCustomerID);
            
            CreateTable(
                "dbo.tbl_Customer",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false, maxLength: 300),
                        PhoneNumber = c.Long(nullable: false),
                        EmailId = c.String(nullable: false, maxLength: 100),
                        CustomerImagePath = c.String(),
                        Password = c.String(nullable: false),
                        AdminId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.tbl_Admin", t => t.AdminId)
                .Index(t => t.PhoneNumber, unique: true)
                .Index(t => t.EmailId, unique: true)
                .Index(t => t.AdminId);
            
            CreateTable(
                "dbo.tbl_Rental",
                c => new
                    {
                        RentalId = c.Int(nullable: false, identity: true),
                        PickupDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(nullable: false),
                        Source = c.String(nullable: false, maxLength: 255),
                        Destination = c.String(nullable: false, maxLength: 255),
                        FairAmount = c.Double(nullable: false),
                        InitialFuel = c.String(nullable: false, maxLength: 255),
                        RentalCustomerId = c.Int(nullable: false),
                        AdminId = c.Int(nullable: false),
                        RentalOwnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RentalId)
                .ForeignKey("dbo.tbl_Admin", t => t.AdminId)
                .ForeignKey("dbo.tbl_Customer", t => t.RentalCustomerId)
                .ForeignKey("dbo.tbl_Owner", t => t.RentalOwnerId)
                .Index(t => t.RentalCustomerId)
                .Index(t => t.AdminId)
                .Index(t => t.RentalOwnerId);
            
            CreateTable(
                "dbo.tbl_Owner",
                c => new
                    {
                        OwnerId = c.Int(nullable: false, identity: true),
                        OwnerName = c.String(nullable: false, maxLength: 50),
                        Address = c.String(nullable: false, maxLength: 300),
                        PhoneNumber = c.Long(nullable: false),
                        EmailId = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false),
                        TotalEarnings = c.Double(),
                        OwnerImagePath = c.String(),
                        AdminId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OwnerId)
                .ForeignKey("dbo.tbl_Admin", t => t.AdminId)
                .Index(t => t.PhoneNumber, unique: true)
                .Index(t => t.EmailId, unique: true)
                .Index(t => t.AdminId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_Car", "CarRentalId", "dbo.tbl_Rental");
            DropForeignKey("dbo.tbl_Car", "OwnerId", "dbo.tbl_Owner");
            DropForeignKey("dbo.tbl_Car", "CarCustomerID", "dbo.tbl_Customer");
            DropForeignKey("dbo.tbl_Rental", "RentalOwnerId", "dbo.tbl_Owner");
            DropForeignKey("dbo.tbl_Owner", "AdminId", "dbo.tbl_Admin");
            DropForeignKey("dbo.tbl_Rental", "RentalCustomerId", "dbo.tbl_Customer");
            DropForeignKey("dbo.tbl_Rental", "AdminId", "dbo.tbl_Admin");
            DropForeignKey("dbo.tbl_Customer", "AdminId", "dbo.tbl_Admin");
            DropForeignKey("dbo.tbl_Car", "AdminId", "dbo.tbl_Admin");
            DropIndex("dbo.tbl_Owner", new[] { "AdminId" });
            DropIndex("dbo.tbl_Owner", new[] { "EmailId" });
            DropIndex("dbo.tbl_Owner", new[] { "PhoneNumber" });
            DropIndex("dbo.tbl_Rental", new[] { "RentalOwnerId" });
            DropIndex("dbo.tbl_Rental", new[] { "AdminId" });
            DropIndex("dbo.tbl_Rental", new[] { "RentalCustomerId" });
            DropIndex("dbo.tbl_Customer", new[] { "AdminId" });
            DropIndex("dbo.tbl_Customer", new[] { "EmailId" });
            DropIndex("dbo.tbl_Customer", new[] { "PhoneNumber" });
            DropIndex("dbo.tbl_Car", new[] { "CarCustomerID" });
            DropIndex("dbo.tbl_Car", new[] { "AdminId" });
            DropIndex("dbo.tbl_Car", new[] { "CarRentalId" });
            DropIndex("dbo.tbl_Car", new[] { "OwnerId" });
            DropIndex("dbo.tbl_Car", new[] { "CarNumber" });
            DropIndex("dbo.tbl_Admin", new[] { "EmailId" });
            DropIndex("dbo.tbl_Admin", new[] { "PhoneNumber" });
            DropTable("dbo.tbl_Owner");
            DropTable("dbo.tbl_Rental");
            DropTable("dbo.tbl_Customer");
            DropTable("dbo.tbl_Car");
            DropTable("dbo.tbl_Admin");
        }
    }
}
